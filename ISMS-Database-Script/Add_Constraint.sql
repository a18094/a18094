use [ISMS-IPD16];
go

-- Add FK from tblparents to tblreports
alter table tblreports
	add constraint fk_reportToParents  foreign key (reportTo) references tblparents(pID);
go


-- Add FK from tblemployees to tblrequestreplies
alter table tblrequestreplies
	add constraint fk_empToreply  foreign key (eID) references tblemployees(eID);
go


-- Add FK from tblrequests to tblrequestreplies
alter table tblrequestreplies
	add constraint fk_requestToreply  foreign key (reqID) references tblrequests(reqID);
go

-- Add FK from tblparents to tblrequests
alter table tblrequests
	add constraint fk_parentsTOreqquest  foreign key (pID) references tblparents(pID);
go

-- Add FK from tblstudents to tblrequests
alter table tblrequests
	add constraint fk_studentsTOrequests  foreign key (sID) references tblstudents(sID);
go

-- Add FK from tblparents to tblstudents
alter table tblstudents
	add constraint fk_parentTOstudents foreign key (pID) references tblparents(pID);
go


--Add FK from tblstudents to tblactstudents
alter table tblactstudents
	add constraint fk_stuTOactstudents foreign key (sID) references tblstudents(sID);
go

--Add FK from tblactivities to tblactstudents
alter table tblactstudents
	add constraint fk_actTOactstudents foreign key (actID) references tblactivities(actID);
go

-- Add CHECK constraint for tblemployees
alter table tblemployees
	add constraint CK_eTitle CHECK(eTitle IN ('Manager','Employee'));
go

-- Add CHECK constraint for tblrequests
alter table tblrequests
	add constraint CK_reqType CHECK(reqType IN ('Important','Notice','Reminder','Normal'));
go

alter table tblrequests
	add constraint CK_reqStatus CHECK(reqStatus IN ('InProgress','Waiting','Pending','Finished'));
go

alter table tblreports
	add constraint CK_rptype CHECK(rptType IN ('Weekly','BiWeekly','Monthly','Other'));
go

