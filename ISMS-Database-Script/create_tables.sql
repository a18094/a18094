use [ISMS-IPD16];
go

if OBJECT_ID('tblactivities','U') is not null
drop table tblactivities
;
go

create table tblactivities
(
actID smallint identity(1,1) not null,
actTheme nvarchar(100) not null,
actContent nvarchar(500) null,
actPubDate datetime not null,
actActDate datetime not null,
actPlace nvarchar(200) not null,
actCostPerPerson nvarchar(50) not null,
actMaxNums smallint not null,
actRegisteredNums smallint not null
constraint pk_tblactivities primary key clustered(actID asc)
)
;
go

if OBJECT_ID('tblactstudents','U') is not null
drop table tblactstudents
;
go

create table tblactstudents
(
actstuID 	 smallint identity(1,1) not null,
actID smallint not null,
sID smallint not null,
constraint pk_tblactstudents primary key clustered(actstuID asc)
)
;
go


/*if OBJECT_ID('tblcompanyinfo','U') is not null
drop table tblcompanyinfo
;
go

create table tblcompanyinfo
(
cID 	 smallint identity(1,1) not null,
cName  nvarchar(40) not null,
cAddress nvarchar(100) not null,
cEmail nvarchar(40) not null,
cPhone nvarchar(20) not null,
cContactPerson nvarchar(40) not null,
constraint pk_tblcompanyinfo primary key clustered(cID asc)
)
;
go*/

if OBJECT_ID('tblemployees','U') is not null
drop table tblemployees
;
go

create table tblemployees
(
eID 	 smallint identity(1,1) not null,
eAccount  nvarchar(40) not null,
ePassword nvarchar(100) not null,
eName nvarchar(40) not null,
eTitle nvarchar(20) not null,  -- constraint check (eTitle in ('Manager','Employee'))
ePhone nvarchar(20) not null,
eAddress nvarchar(100) not null,
eEmail nvarchar(40) not null,

constraint pk_tblemployees primary key clustered(eID asc)
)
;
go

if OBJECT_ID('tblparents','U') is not null
drop table tblparents
;
go

create table tblparents
(
pID 	 smallint identity(1,1) not null,
pAccount  nvarchar(40) not null,
pPassword nvarchar(100) not null,
pName nvarchar(40) not null,
pPhone nvarchar(20) not null,
pAddress nvarchar(100) not null,
pEmail nvarchar(40) not null,
pContractNumber int not null,
constraint pk_tblparents primary key clustered(pID asc)
)
;
go



if OBJECT_ID('tblreports','U') is not null
drop table tblreports
;
go

create table tblreports
(
rptID 	 smallint identity(1,1) not null,
rptType  nvarchar(20) not null,
rptSubject nvarchar(50) not null,
rptContent nvarchar(2000) not null,
rptDate datetime not null,
reportTo smallint not null, -- FK (reportTo -> pID)
constraint pk_tblreports primary key clustered(rptID asc)
)
;
go

if OBJECT_ID('tblrequests','U') is not null
drop table tblrequests
;
go

create table tblrequests
(
reqID   int identity(1,1) not null,
pID		smallint not null,
sID		smallint not null,
reqType nvarchar(20) not null, -- constraint check(reqType IN ('Important','Notice','Reminder','Normal'))
reqsubject	nvarchar(50) not null,
reqContent	nvarchar(2000) not null,
reqDate datetime not null,
reqStatus nvarchar(20) not null -- constraint check(reqStatus IN ('In Progress','Waiting','Pending','Finished'))

constraint pk_tblrequests primary key clustered(reqID asc)
)
;
go


if OBJECT_ID('tblrequestreplies','U') is not null
drop table tblrequestreplies
;
go

create table tblrequestreplies
(
replyID	int identity(1,1) not null,
reqID   int not null,  -- FK
eID 	smallint not null,  -- FK
replyContent	nvarchar(2000) not null,
constraint pk_tblrequestreplies primary key clustered(replyID asc)
)
;
go


if OBJECT_ID('tblstudents','U') is not null
drop table tblstudents
;
go

create table tblstudents
(
sID 	 smallint identity(1,1) not null,
pID 	 smallint not null,  -- FK
sAccount  nvarchar(40) not null,
sPassword nvarchar(100) not null,
sName nvarchar(40) not null,
sPhone nvarchar(20) not null,
sAddress nvarchar(100) not null,
sEmail nvarchar(40) not null,
sPassportNum nvarchar(20) not null,
constraint pk_tblstudents primary key clustered(sID asc)
)
;
go


