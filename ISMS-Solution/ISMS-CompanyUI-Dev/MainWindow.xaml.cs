﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace ISMS_CompanyUI_Dev
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // For database functions
        Database db = new Database();

        // For CommonFunctions
        //CommonFunctions commonFunc = new CommonFunctions();

        //Default, Just allow 5 times for login retry        
        public int LoginTimes = 5;
        public int leftRetryTimes;
        public MainWindow()
        {
            InitializeComponent();
            this.ShowDialog();

   

        }

        private void Application_Exit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);

        }

        private void btnExitLogin_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult msgResult = MessageBox.Show("Do you want to quit ISMS program?", "ISMS Message Box", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (msgResult == MessageBoxResult.Yes)
            {

                Environment.Exit(0);
            }
            else { return; }
        }
       // Database db = new Database();
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            if (LoginTimes > 1)
            {
                if (txtAccount.Text == "" || txtAccount.Text.Contains(';'))
                {
                    leftRetryTimes = LoginTimes - 1;
                    LoginTimes--;
                    MessageBox.Show("Account is empty or contains semicolo. You still have " + leftRetryTimes + " times to try!");
                    return;
                }

                if (txtPWD.Password.Length < 6 || txtPWD.Password.Length > 20)
                {

                    leftRetryTimes = LoginTimes - 1;
                    LoginTimes--;
                    MessageBox.Show("Password must be between 6-20 characters long!  You still have " + leftRetryTimes + " times to try!");
                    return;
                }
                try
                {
        
                    // Convert password to MD5 String
                    string md5PWD = CommonFunctions.str2md5(txtPWD.Password);
 
                    //Verify login account and password in database
                    int eid = db.LoginVerification(txtAccount.Text.ToString(), md5PWD, "tblEmployees");
                    if (eid > 0)
                    {
                        //MessageBox.Show("Login successfully, ID is " + eid.ToString());
                        //Login successfully. Hide login Window

                        //Store Account and AccountID to gloabl vars
                        CommonFunctions.loggedInAccountID = eid;
                        CommonFunctions.loggedInAccount = txtAccount.Text;

                        this.Hide();
                        //DialogResult = true;
                       // this.Hide(); //Hide login window                    


                    }
                    else
                    {
                        leftRetryTimes = LoginTimes - 1;
                        LoginTimes--;
                        MessageBox.Show("Account or Password authentication failed! You still have " + leftRetryTimes + " times to try!");
                        return;
                    }
                }
                catch (Exception ex)
                {

                    if (ex is EncoderFallbackException || ex is InvalidOperationException
                        || ex is ObjectDisposedException || ex is ArgumentNullException || ex is SqlException)

                    {

                        MessageBox.Show("Encrypt password errors! " + ex.Message, "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Error);

                    Environment.Exit(0);

                    }
                }

            }
            else
            {

                //TODO: exit if login retry more than 5 times!          

                MessageBox.Show("Your login failed  too many times. Program exit!", "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }
        
        }
    }
}
