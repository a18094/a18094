﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ISMS_CompanyUI_Dev
{
    /// <summary>
    /// Interaction logic for ActivityWindow.xaml
    /// </summary>
    public partial class ActivityWindow : Window
    {
        Database db = new Database();

        string strSubject, strContent, strCost, strPlace;
        DateTime actDate, currentDate;
        int MaxNo, regNo;
        public ActivityWindow()
        {
            InitializeComponent();

            dp.SelectedDate = CommonFunctions.selectedDateTime;
        }

        
        private void BtkOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //string strSubject, strContent, strCost, strPlace;
                //DateTime actDate, currentDate;
                //int MaxNo, regNo;

                //Just need to check if acitivityDate > today

                currentDate = DateTime.Now;
                DateTime ActivityDate = dp.SelectedDate.Value;
                TimeSpan span = ActivityDate - currentDate;
                if (span.TotalHours <= 0.0)
                {
                    MessageBox.Show("You should select a future date to publish new activity!", "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (txtSubject.Text == "" ) {

                    MessageBox.Show("Subject must be between 1-50 characters long!");
                    return;

                }


                if (txtContent.Text == "")
                {

                    MessageBox.Show("Content must be between 1-500 characters long!");
                    return;

                }

                if (txtContent.Text == "")
                {

                    MessageBox.Show("Content must be between 1-500 characters long!");
                    return;

                }

                //Cost must contain at least 1 number and no semicolon
                string costPat = @"^.*[0-9]+.*$";
                Regex rx = new Regex(costPat);

                if (txtCost.Text == "" || !rx.IsMatch(txtCost.Text)|| txtCost.Text.Contains(';')) {

                    MessageBox.Show("Cost must have at least 1 number and no semicolon!");
                    return;
                }

                if (dp.SelectedDate == null) {

                    MessageBox.Show("Activity Date can NOT be empty!");
                    return;

                }

                string maxPat = @"^([1-9]+|[1-9][0-9]+)+$";
                Regex maxRX = new Regex(maxPat);

                if (txtMax.Text == "" || !maxRX.IsMatch(txtMax.Text.ToString())) {
                    MessageBox.Show("Max Number of Places must be a number and greater than 1!");
                    return;

                }

                string regPat = @"^([0-9]+|[1-9][0-9]+)+$";
                Regex regRX = new Regex(regPat);

                if (txtReg.Text == "" || !regRX.IsMatch(txtReg.Text.ToString()) )
                {
                    MessageBox.Show("Registered number must be a number and between 0 to " + txtMax.Text);
                    return;

                }


                //Input values are OK now!
                strSubject = txtSubject.Text.ToString();
                strContent = txtContent.Text.ToString();
                strCost = txtCost.Text.ToString();
                strPlace = txtPlace.Text.ToString();
                actDate = dp.SelectedDate.Value;
                //currentDate  //This  field is current local date
                MaxNo = int.Parse(txtMax.Text); 
                regNo = int.Parse(txtReg.Text); 

 
                //Call InsertAct()
                int returnValue = db.InserAct(new newActivity(strSubject, strContent, strCost,
                    strPlace, actDate, currentDate, MaxNo, regNo));

                if (returnValue > 0)
                {

                    MessageBox.Show("Insert new record in database successfully!");

                    //Clean current window after insert successful
                    txtSubject.Text = "";
                    txtContent.Text = "";
                    txtCost.Text = "";
                    txtPlace.Text = "";
                    txtMax.Text = "";
                    txtReg.Text = "";

                    return;


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Errors: Failed to add new record to Database! " + ex.Message);
                return;


            }


        }

        private void BtnCacel_Click(object sender, RoutedEventArgs e)
        {

            if (txtSubject.Text != "" || txtSubject.Text != "" || txtPlace.Text != "" ||
                txtCost.Text != "" || txtMax.Text != "" || txtReg.Text != "")
            {

                MessageBoxResult result = MessageBox.Show("Close the window will lose unsaved data", "ISMS Message Box", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                if (result == MessageBoxResult.OK)
                {

                    this.Close();

                }
                else
                {

                    return;
                }

            }
            else { this.Close(); }

        }
    }
}
