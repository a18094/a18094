﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ISMS_CompanyUI_Dev
{
    /// <summary>
    /// Interaction logic for CompanyMainWindow.xaml
    /// </summary>
    public partial class CompanyMainWindow : Window
    {
        Database db = new Database();
        //CommonFunctions commonFunc = new CommonFunctions();

        int LVCounter;



        public CompanyMainWindow()
        {
            InitializeComponent();

            //BindDatatoListview();
            BindDataToTreeview();

            //Initlize some stuffs in main window
            lbListViewCounter.Text = LVCounter.ToString() + " records found!";

        }

        List<RequestsForListView> RequestView = new List<RequestsForListView>();
        public void BindDatatoListview()
        {

            //List<RequestsForListView> RequestView = new List<RequestsForListView>();

            //Fetch data from DB then bind to view
            RequestView = db.GetAllReuqests();
            LVCounter = RequestView.Count();
            lv.ItemsSource = RequestView;
            lv.Items.Refresh();


        }

        public void BindDataToTreeview()
        {
            tv.Items.Refresh();
            //TODO: try{} catch{}
            List<string> pNameList = new List<string>();
            List<string> recordList = new List<string>();
            recordList = db.GetAllParentStudentName();

            string pName = "";
            foreach (string item in recordList)
            {

                string pname, parentID, childname, childID;



                pname = item.ToString().Split(';')[0];  //[0] is parent name
                parentID = item.ToString().Split(';')[1]; // [1] is parent ID



                childname = item.ToString().Split(';')[2]; //[2] is student name
                childID = item.ToString().Split(';')[3];  // [3] is student ID

                if (pName != pname)
                {


                    pName = pname;
                    pNameList.Add(pName + "," + parentID);

                }
                else
                {

                    continue;
                }

            }

            //Bing data to Treeview
            MenuItem root = new MenuItem() { Title = "Customer View" };
            foreach (string p in pNameList)
            {
                string newpname = p.Split(',')[0];
                string newPID = p.Split(',')[1];
                string newpStr = newpname + "(pID:" + newPID + ")";
                // Example: TomWang(pID:2)
                MenuItem childItem1 = new MenuItem() { Title = newpStr };

                foreach (string stu in recordList)
                {


                    if (newpname == stu.ToString().Split(';')[0])
                    {
                        string newstuname = stu.ToString().Split(';')[2];
                        string newstuID = stu.ToString().Split(';')[3];
                        string newstuStr = newstuname + "(sID:" + newstuID + ")";
                        //Example: Wang aaaa(sID: 2)
                        childItem1.Items.Add(new MenuItem() { Title = newstuStr });

                    }
                    else
                    {
                        continue;
                    }

                }

                root.Items.Add(childItem1);
            }
            tv.Items.Add(root);

        }

        public class MenuItem
        {
            public MenuItem()
            {
                this.Items = new ObservableCollection<MenuItem>();
            }

            public string Title { get; set; }

            public ObservableCollection<MenuItem> Items { get; set; }
        }


        private void CompanyMainExit_Click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult msgResult = MessageBox.Show("Do you want to quit ISMS program?", "ISMS Message Box", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (msgResult == MessageBoxResult.Yes)
            {

                Environment.Exit(0);
            }
            else { return; }
        }

        private void tv_Selected(object sender, RoutedEventArgs e)
        {
            //TODO: try{} catch{}
            try
            {
                TreeViewItem tvi = e.OriginalSource as TreeViewItem;
                var node = tvi.Header as MenuItem;


                if (node != null && node.Title.ToString() != "Customers")
                {
                    //MessageBox.Show(node.Title.ToString());
                    string selectedName = node.Title.ToString();

                    //MessageBox.Show(selectedName);
                    string[] newstr = selectedName.Split('(')[1].Split(':');
                    switch (newstr[0].ToLower())
                    {

                        case "pid":
                            CommonFunctions.tvSelectedAccountType = "tblparents";
                            break;
                        case "sid":
                            CommonFunctions.tvSelectedAccountType = "tblstudents";
                            break;
                        case "eid":
                            CommonFunctions.tvSelectedAccountType = "tblemployees";
                            break;
                        default:
                            break;
                    }

                    string id = newstr[1].Replace(")", "");

                    //MessageBox.Show("ID:" + id + " table: " + CommonFunctions.tvSelectedAccountType);
                    CommonFunctions.tvSelectedAccountID = int.Parse(id);

                    //Select all requests related to selectID from treeview, then show in listview
                    List<RequestsForListView> ReqByIDList = new List<RequestsForListView>();

                    if (CommonFunctions.tvSelectedAccountType.ToString().ToLower() == "tblparents")
                    {

                        foreach (RequestsForListView r in RequestView)
                        {

                            if (r.pID == CommonFunctions.tvSelectedAccountID)
                            {

                                ReqByIDList.Add(r);
                            }

                        }

                    }
                    else if (CommonFunctions.tvSelectedAccountType.ToString().ToLower() == "tblstudents")
                    {

                        foreach (RequestsForListView r in RequestView)
                        {

                            if (r.sID == CommonFunctions.tvSelectedAccountID)
                            {

                                ReqByIDList.Add(r);
                            }

                        }

                    }

                    if (ReqByIDList.Count() > 0)
                    {

                        lbListViewCounter.Text = ReqByIDList.Count().ToString() + " records found!";
                        lv.ItemsSource = ReqByIDList;
                        lv.Items.Refresh();

                    }

                }


                else
                {

                    return;
                }
            }
            catch (Exception ex)
            {

                if (ex is ArgumentNullException || ex is ArgumentOutOfRangeException || ex is ArgumentException || ex is IndexOutOfRangeException || ex is FormatException)
                {

                    //For above Exceptions, No need to response!!
                }
                else
                {

                    MessageBox.Show("Unknown errors! " + ex.Message);

                }


            }
        }

        private void cal_MouseEnter(object sender, MouseEventArgs e)
        {
            txtSelectedDate.Text = "Click Calender Add Activity";
        }


        private void cal_MouseLeave(object sender, MouseEventArgs e)
        {
            txtSelectedDate.Text = "";
        }


        private void Cal_selectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cal.SelectedDate.HasValue)
            {
                CommonFunctions.selectedDateTime = cal.SelectedDate.Value;

                //MessageBox.Show(CommonFunctions.selectedDateTime.ToString());

                ActivityWindow aw = new ActivityWindow();
                aw.ShowDialog();

            }
            else
            {


                MessageBox.Show("Have to select a date before publish new activity!");
                return;

            }

        }



        private void ReloadAllRequest_Click(object sender, RoutedEventArgs e)
        {
            BindDatatoListview();
            lbListViewCounter.Text = LVCounter.ToString() + " records found!";

        }

        private void ReplyLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        List<RequestsReplies> replyList = new List<RequestsReplies>();
        private void Lv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                //MessageBox.Show(lv.SelectedItem.ToString());
                string reqID, reqCont;
                string[] selectedItemArr;
                if (lv.SelectedItem != null)
                {

                    selectedItemArr = lv.SelectedItem.ToString().Split(',').ToArray();
                    txtReqID.Text = selectedItemArr[0].ToString();//Load req ID to text Box
                    reqID = selectedItemArr[0].ToString();
                    txtRepToreqID.Text = selectedItemArr[0].ToString();//Load req ID to replyTo box


                    txtReqType.Text = selectedItemArr[1].ToString(); //Load req type to text Box
                    reqCont = selectedItemArr[1].ToString();

                    if (selectedItemArr[1].ToString() == "important")
                    {

                        txtReqType.Background = Brushes.Red;
                    }
                    else
                    {
                        txtReqType.Background = Brushes.LightGray;

                    }
                    txtReq.Text = selectedItemArr[3].ToString(); // Load req content to text box



                    //Create a new list bind to  reply Listview
                    //List<RequestsReplies> replyList = new List<RequestsReplies>();

                    replyList = db.GetAllRepliesOnGivenRequestID(int.Parse(reqID));

                    if (replyList.Count > 0)
                    {
                        replyLV.ItemsSource = replyList;
                        replyLV.Items.Refresh();
                        txtNumberOfReply.Text = (replyList.Count.ToString());
                    }
                    else
                    {

                        replyLV.ItemsSource = null;

                        replyLV.Items.Refresh();

                        txtNumberOfReply.Text = "0";

                    }



                }
            }
            catch (Exception ex)
            {

                if (ex is ArgumentNullException || ex is ArgumentOutOfRangeException || ex is ArgumentException || ex is IndexOutOfRangeException || ex is FormatException)
                {

                    //For above Exceptions, No need to response!!
                }
                else
                {

                    MessageBox.Show("Unknown errors! " + ex.Message);

                }

            }


        }

        private void btnReply_Click(object sender, RoutedEventArgs e)
        {
            int reqid, eid;
            string reqCont;
            try
            {
                //MessageBox.Show(lv.SelectedItem.ToString());

                if (txtRepToreqID.Text != "")
                {

                    reqid = int.Parse(txtRepToreqID.Text);
                    reqCont = txtReply.Text.ToString();
                    eid = int.Parse(CommonFunctions.loggedInAccountID.ToString());
                    DateTime repliedDate = DateTime.Today.Date;


                    //Insert reply to DB
                    int result = db.InsertReply(new RequestsReplies() { reqID = reqid, eID = eid, replyContent = reqCont, issuedDate = repliedDate });

                    if (result > 0)
                    {

                        MessageBox.Show("Reply successful!");
                        //Clean old reply content
                        txtReply.Text = "";
                        // rebind data and refresh for reply Listview
                        replyList = db.GetAllRepliesOnGivenRequestID(reqid);
                        replyLV.ItemsSource = replyList;
                        replyLV.Items.Refresh();
                        return;

                    }
                    else
                    {

                        MessageBox.Show("Reply failed!");
                        return;
                    }

                }
                else
                {

                    MessageBox.Show("Please select a request you want to reply!");
                    lv.Focus();
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fatal Errors: " + ex.Message);
                return;
            }
        }

        private void btnClean_Click(object sender, RoutedEventArgs e)
        {

            if (txtReply.Text != "")
            {

                MessageBoxResult result = MessageBox.Show("Cancel will lose unsaved data!", "ISMS Message Box", MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if (result == MessageBoxResult.Yes)
                {

                    txtReply.Text = ""; // agree to clean. Do clean
                }
                else
                {

                    return;
                }

            }

        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void AccountMgtTools_Click(object sender, RoutedEventArgs e)
        {
            AccountMgtTools AccountMgtToolWindow = new AccountMgtTools();

            AccountMgtToolWindow.ShowDialog();
        }

        private void AddAct_Click(object sender, RoutedEventArgs e)
        {


            if (cal.SelectedDate.HasValue)
            {
                CommonFunctions.selectedDateTime = cal.SelectedDate.Value;

                //MessageBox.Show(CommonFunctions.selectedDateTime.ToString());

                ActivityWindow aw = new ActivityWindow();
                aw.ShowDialog();

            }
            else
            {


                //MessageBox.Show("Remider: You should select a future date to publish new activity!");
                //return;

                MessageBox.Show("You should select a future date to publish new activity!", "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Information);
                CommonFunctions.selectedDateTime = DateTime.Today.Date;
                ActivityWindow aw = new ActivityWindow();
                aw.ShowDialog();

            }

        }

        private void ActView_Click(object sender, RoutedEventArgs e)
        {
            ActivityView actView = new ActivityView();
            actView.ShowDialog();
        }
    }
}

