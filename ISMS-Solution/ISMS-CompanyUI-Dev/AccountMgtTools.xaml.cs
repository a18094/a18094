﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ISMS_CompanyUI_Dev
{
    /// <summary>
    /// Interaction logic for AccountMgtTools.xaml
    /// </summary>
    public partial class AccountMgtTools : Window
    {
        Database db = new Database();
        public AccountMgtTools()
        {
            InitializeComponent();

            //Binding Data to Listview
            BindDataToAccountListView();
        }


        // Define list for employee, parent and student

        List<Employees> empList = new List<Employees>();
        List<Parents> parentList = new List<Parents>();
        List<Students> stuList = new List<Students>();

        public void BindDataToAccountListView()
        {
            try
            {
                parentList = db.getParentAccountInfo();
                empList = db.getEmpAccountInfo();
                stuList = db.getStuAccountInfo();

                string tblName;
                if (rbPar.IsChecked == true)
                {
                    tblName = "tblparents";
                    lbSwitch.Content = "Contract No.:";
                    //parentList = db.getParentAccountInfo();
                    accountLV.ItemsSource = parentList;



                }
                else if (rbEmp.IsChecked == true)
                {

                    tblName = "tblemployees";
                    lbSwitch.Content = "Title:";
                    //empList = db.getEmpAccountInfo();
                    accountLV.ItemsSource = empList;

                }
                else
                {

                    tblName = "tblstudents";
                    lbSwitch.Content = "Passport No.:";
                    //stuList = db.getStuAccountInfo();
                    accountLV.ItemsSource = stuList;
                }

                accountLV.Items.Refresh();

            }
            catch (Exception ex)
            {


                MessageBox.Show("Errors: " + ex.Message);

            }



        }



        private void AccountWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch { }
        }


        private void accountLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (accountLV.SelectedItem != null && rbPar.IsChecked == true)
            {

                //Parents p = accountLV.SelectedItem as Parents;
                //MessageBox.Show(p.ID.ToString());



            }
        }

        private void rb_click(object sender, RoutedEventArgs e)
        {
            //Switch date source for listview depends on which kind of account selected
            BindDataToAccountListView();

        }


        //----------------------------------------------------
        //Common vars for employees, parents and students
        string account, pwd, name, phone, address, email, tblName;

        // Specified vars for different type accounts
        //string title, PassportNo;

        //int ContractNo;
        //----------------------------------------------------
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (rbPar.IsChecked == true)
                {
                    tblName = "tblparents";
                    //ContractNo = int.Parse(txtSwitch.Text.ToString());
                    if (txtSwitch.Text.ToString().Length < 1 || txtSwitch.Text.ToString().Length > 30)
                    {

                        throw new Exception("Parent Contract number must be between 1-30 characters long!");

                    }

                }
                else if (rbEmp.IsChecked == true)
                {
                    tblName = "tblemployees";
                    //title = txtSwitch.Text.ToString();
                    if (txtSwitch.Text.ToString().ToLower() != "employee" && txtSwitch.Text.ToString().ToLower() != "manager")
                    {

                        throw new Exception("Employee title should be Employee or Manager!");
                    }
                }
                else
                {
                    tblName = "tblstudents";
                    //PassportNo = txtSwitch.Text.ToString();

                    if (txtSwitch.Text.ToString() == "")
                    {

                        throw new Exception("Passport number can NOT be empty!");
                    }

                }

                //Check All from input box

                // Account
                if (txtAccount.Text.Length < 1 || txtAccount.Text.Length > 40)
                {
                    MessageBox.Show("Account must be between 1-40 characters long!");
                    return;

                }
                else
                {

                    account = txtAccount.Text.ToString();
                }

                // Name
                if (txtName.Text.Length < 1 || txtName.Text.Length > 40)
                {
                    MessageBox.Show("Name must be between 1-40 characters long!");
                    return;

                }
                else
                {

                    name = txtName.Text.ToString();
                }

                //Email
                string emailPat = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
                Regex rx = new Regex(emailPat);
                if (txtEmail.Text.Length < 5 || txtEmail.Text.Length > 40 || !rx.IsMatch(txtEmail.Text.ToString()))
                {
                    MessageBox.Show("Invalid Email length(5-40 charaters) or format!");
                    return;

                }
                else
                {

                    email = txtEmail.Text.ToString();
                }

                //Title or ContractNo or PassportNo verified already


                //Phone
                string phonePat = @"^.*[0-9]+.*$";
                Regex phoneRX = new Regex(phonePat);

                if (txtPhone.Text.Length < 5 ||
                    txtPhone.Text.Length > 20 ||
                    txtPhone.Text.ToString().Contains(';') ||
                    txtPhone.Text.ToString().Contains(',') ||
                    !phoneRX.IsMatch(txtPhone.Text.ToString())
                   )

                {
                    MessageBox.Show("Phone must have at least 1 digital and  between in 5-20 characters long!");
                    return;

                }
                else
                {

                    phone = txtPhone.Text.ToString();
                }

                //Address
                if (txtAddress.Text.Length < 1 || txtAddress.Text.Length > 100)
                {
                    MessageBox.Show("Address must be between 1-100 characters long!");
                    return;

                }
                else
                {

                    address = txtAddress.Text.ToString();
                }

                //Passord
                if (txtPWD1.Password.Length < 6 || txtPWD1.Password.Length > 20)
                {
                    MessageBox.Show("Password1 must be between 6-20 characters long!");
                    return;

                }

                if (txtPWD2.Password.Length < 6 || txtPWD2.Password.Length > 20)
                {
                    MessageBox.Show("Password2 must be between 6-20 characters long!");
                    return;

                }

                if (txtPWD1.Password.ToString() != txtPWD2.Password.ToString())
                {

                    MessageBox.Show("You input 2 times password mismatched. Please input again!");
                    return;

                }

                //Convert correct password string to MD5 string
                pwd = CommonFunctions.str2md5(txtPWD1.Password.ToString());


                //Put common data to People class(base class), construct Arguments line
                // Create people list
                People OnePeople = new People()
                {

                    Account = account,
                    Password = pwd,
                    Name = name,
                    Phone = phone,
                    Address = address,
                    Email = email,

                };


                //Call DB Function Add/Update account for 3 types account
                // tblemployees / tblparents / tblstudents
                //TODO: Student account contains pID for.             
                int returnValue = db.InsertUpdateAccount(tblName, OnePeople, txtSwitch.Text.ToString());

                if (returnValue > 0)
                {

                    MessageBox.Show("Add/Update Account Successfully!");

                    //Clean everything in input box
                    txtAccount.Text = "";
                    txtAddress.Text = "";
                    txtEmail.Text = "";
                    txtName.Text = "";
                    txtPhone.Text = "";
                    txtPWD1.Password = "";
                    txtPWD2.Password = "";
                    txtSwitch.Text = "";
                    BindDataToAccountListView();

                    return;

                }
                else
                {

                    MessageBox.Show("Add/Update Account Failed!");
                    return;

                }
            }
            catch (Exception ex) {

                MessageBox.Show("Add/Update Account Errors. " + ex.Message);

            }




        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
