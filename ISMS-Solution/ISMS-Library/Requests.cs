﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISMS_Library
{
    //Class: Requests  
    //Map To: tblRequests
    public class Requests : RequestsReportsBase
    {

        public Requests(): base() { }

        // public enum reqType { Important, Notice, Reminder, Normal }
        public string reqType { set; get; }

        //public enum reqStatus { InProgress, Waiting, Pending, Finished }
        public string reqStatus { set; get; }

        //public int replyCount { set; get; }

        public override string ToString()
        {
            return string.Format("{0}. Title: {1}\n Content: {2} \n Issued on: {3} \n Type: {4}\n Status: {5}\n ", ID, subject, Content, rDate, reqType, reqStatus);
        }

        public string ToShowSubject()
        {
            return string.Format("{0} issued on {1} and total replies number is  ", subject, rDate);
        }
    }


    public class RequestsForListView : Requests{

        public RequestsForListView() : base() { }

        public RequestsForListView(int reqID, string reqtype,string reqSubject, string reqContent,

           string reqDate, string reqstatus,int reqpID, int reqsID){

            this.ID = reqID;
            this.subject = reqSubject;
            this.Content = reqContent;
            this.rDate = DateTime.Parse(reqDate);
            this.reqStatus = reqstatus;
            this.reqType = reqtype;
            this.pID = reqpID; // if pID = 0, means pID not found , value= 0 or >0
            this.sID = reqsID; // if sID = 0, means sID not found,  value = 0 or > 0


            //repCount = replyCount; // Not created in database yet!

        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ID, reqType, subject, Content, rDate, reqStatus,pID,sID);
        }

    }

    public class RequestsShowShort : Requests
    {

        public RequestsShowShort() : base() { }

        public RequestsShowShort(int reqID,  string reqSubject, DateTime reqDate /*, int replycount*/)
        {
            ID = reqID;
            subject = reqSubject;
            rDate = reqDate;               
            //replyCount = replycount;
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2}", ID, subject, rDate);
        }

    }

}
