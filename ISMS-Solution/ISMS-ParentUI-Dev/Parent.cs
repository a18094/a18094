﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace ISMS_ParentUI_Dev
{
    class Parent:People
    {
        // ContractNumber for Parent
        private string _contractNumber;
        public string ContractNumber
        {
            set
            {
                var reg = new Regex(@"^\d{6}$");
                if (!reg.IsMatch(_contractNumber))
                {
                    MessageBox.Show("Incorrect contract number format! Please enter again!(6 digits)");
                    return;
                }
                else
                {
                    _contractNumber = value;
                }
            }
            get
            {
                return _contractNumber;
            }
        }
    }
}
